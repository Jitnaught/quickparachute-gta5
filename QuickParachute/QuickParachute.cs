﻿using System;
using System.Windows.Forms;
using GTA;
using GTA.Native;

namespace QuickParachute
{
	public class QuickParachute : Script
	{
		readonly Keys giveKey;
		readonly GTA.Control giveButton;

		public QuickParachute()
		{
			giveKey = Settings.GetValue("Settings", "Key", Keys.E);
			giveButton = Settings.GetValue("Settings", "ControllerButton", GTA.Control.Jump);

			KeyDown += Parachute_KeyDown;
			Tick += Parachute_Tick;
		}

		void GiveParachute()
		{
			Ped plrPed = Game.Player.Character;

			if (plrPed != null && plrPed.Exists() && plrPed.IsAlive && plrPed.IsInAir && !plrPed.Weapons.HasWeapon(WeaponHash.Parachute))
			{
				plrPed.Weapons.Give(WeaponHash.Parachute, 1, true, true);
			}
		}

		void Parachute_Tick(object sender, EventArgs e)
		{
			if (Game.CurrentInputMode == InputMode.GamePad && Game.IsControlJustPressed(0, giveButton))
			{
				GiveParachute();
			}
		}

		void Parachute_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == giveKey)
			{
				GiveParachute();
			}
		}
	}
}
